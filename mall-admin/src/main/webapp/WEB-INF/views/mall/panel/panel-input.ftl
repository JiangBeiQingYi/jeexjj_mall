<#--
/****************************************************
 * Description: 内容分类的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/panel/save" id=tabId>
   <input type="hidden" name="id" value="${panel.id}"/>
   
   <@formgroup title='板块名称'>
	<input type="text" name="name" value="${panel.name}" >
   </@formgroup>
   <@formgroup title='类型 0轮播图 1板块种类一 2板块种类二 3板块种类三 '>
	<input type="text" name="type" value="${panel.type}" check-type="number">
   </@formgroup>
   <@formgroup title='排列序号'>
	<input type="text" name="sortOrder" value="${panel.sortOrder}" check-type="number">
   </@formgroup>
   <@formgroup title='所属位置 0首页 1商品推荐 2我要捐赠'>
	<input type="text" name="position" value="${panel.position}" check-type="number">
   </@formgroup>
   <@formgroup title='板块限制商品数量'>
	<input type="text" name="limitNum" value="${panel.limitNum}" check-type="number">
   </@formgroup>
   <@formgroup title='状态'>
	<input type="text" name="status" value="${panel.status}" check-type="number">
   </@formgroup>
   <@formgroup title='备注'>
	<input type="text" name="remark" value="${panel.remark}" >
   </@formgroup>
   <@formgroup title='创建时间'>
	<@date name="created" dateValue=panel.created  default=true/>
   </@formgroup>
   <@formgroup title='更新时间'>
	<@date name="updated" dateValue=panel.updated  default=true/>
   </@formgroup>
</@input>