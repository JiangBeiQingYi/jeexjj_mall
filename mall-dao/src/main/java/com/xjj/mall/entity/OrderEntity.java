/****************************************************
 * Description: Entity for t_mall_order
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.util.Date;
import java.math.BigDecimal;
import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class OrderEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public OrderEntity(){}
    private String orderId;//订单id
    private BigDecimal payment;//实付金额
    private Integer paymentType;//支付类型 1在线支付 2货到付款
    private BigDecimal postFee;//邮费
    private Integer status;//状态 0未付款 1已付款 2未发货 3已发货 4交易成功 5交易关闭 6交易失败
    private Date createTime;//订单创建时间
    private Date updateTime;//订单更新时间
    private Date paymentTime;//付款时间
    private Date consignTime;//发货时间
    private Date endTime;//交易完成时间
    private Date closeTime;//交易关闭时间
    private String shippingName;//物流名称
    private String shippingCode;//物流单号
    private Long userId;//用户id
    private String buyerMessage;//买家留言
    private String buyerNick;//买家昵称
    private Integer buyerComment;//买家是否已经评价
    /**
     * 返回订单id
     * @return 订单id
     */
    public String getOrderId() {
        return orderId;
    }
    
    /**
     * 设置订单id
     * @param orderId 订单id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    /**
     * 返回实付金额
     * @return 实付金额
     */
    public BigDecimal getPayment() {
        return payment;
    }
    
    /**
     * 设置实付金额
     * @param payment 实付金额
     */
    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }
    
    /**
     * 返回支付类型 1在线支付 2货到付款
     * @return 支付类型 1在线支付 2货到付款
     */
    public Integer getPaymentType() {
        return paymentType;
    }
    
    /**
     * 设置支付类型 1在线支付 2货到付款
     * @param paymentType 支付类型 1在线支付 2货到付款
     */
    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }
    
    /**
     * 返回邮费
     * @return 邮费
     */
    public BigDecimal getPostFee() {
        return postFee;
    }
    
    /**
     * 设置邮费
     * @param postFee 邮费
     */
    public void setPostFee(BigDecimal postFee) {
        this.postFee = postFee;
    }
    
    /**
     * 返回状态 0未付款 1已付款 2未发货 3已发货 4交易成功 5交易关闭 6交易失败
     * @return 状态 0未付款 1已付款 2未发货 3已发货 4交易成功 5交易关闭 6交易失败
     */
    public Integer getStatus() {
        return status;
    }
    
    /**
     * 设置状态 0未付款 1已付款 2未发货 3已发货 4交易成功 5交易关闭 6交易失败
     * @param status 状态 0未付款 1已付款 2未发货 3已发货 4交易成功 5交易关闭 6交易失败
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
    
    /**
     * 返回订单创建时间
     * @return 订单创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }
    
    /**
     * 设置订单创建时间
     * @param createTime 订单创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    /**
     * 返回订单更新时间
     * @return 订单更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }
    
    /**
     * 设置订单更新时间
     * @param updateTime 订单更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    
    /**
     * 返回付款时间
     * @return 付款时间
     */
    public Date getPaymentTime() {
        return paymentTime;
    }
    
    /**
     * 设置付款时间
     * @param paymentTime 付款时间
     */
    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }
    
    /**
     * 返回发货时间
     * @return 发货时间
     */
    public Date getConsignTime() {
        return consignTime;
    }
    
    /**
     * 设置发货时间
     * @param consignTime 发货时间
     */
    public void setConsignTime(Date consignTime) {
        this.consignTime = consignTime;
    }
    
    /**
     * 返回交易完成时间
     * @return 交易完成时间
     */
    public Date getEndTime() {
        return endTime;
    }
    
    /**
     * 设置交易完成时间
     * @param endTime 交易完成时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    
    /**
     * 返回交易关闭时间
     * @return 交易关闭时间
     */
    public Date getCloseTime() {
        return closeTime;
    }
    
    /**
     * 设置交易关闭时间
     * @param closeTime 交易关闭时间
     */
    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }
    
    /**
     * 返回物流名称
     * @return 物流名称
     */
    public String getShippingName() {
        return shippingName;
    }
    
    /**
     * 设置物流名称
     * @param shippingName 物流名称
     */
    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }
    
    /**
     * 返回物流单号
     * @return 物流单号
     */
    public String getShippingCode() {
        return shippingCode;
    }
    
    /**
     * 设置物流单号
     * @param shippingCode 物流单号
     */
    public void setShippingCode(String shippingCode) {
        this.shippingCode = shippingCode;
    }
    
    /**
     * 返回用户id
     * @return 用户id
     */
    public Long getUserId() {
        return userId;
    }
    
    /**
     * 设置用户id
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    /**
     * 返回买家留言
     * @return 买家留言
     */
    public String getBuyerMessage() {
        return buyerMessage;
    }
    
    /**
     * 设置买家留言
     * @param buyerMessage 买家留言
     */
    public void setBuyerMessage(String buyerMessage) {
        this.buyerMessage = buyerMessage;
    }
    
    /**
     * 返回买家昵称
     * @return 买家昵称
     */
    public String getBuyerNick() {
        return buyerNick;
    }
    
    /**
     * 设置买家昵称
     * @param buyerNick 买家昵称
     */
    public void setBuyerNick(String buyerNick) {
        this.buyerNick = buyerNick;
    }
    
    /**
     * 返回买家是否已经评价
     * @return 买家是否已经评价
     */
    public Integer getBuyerComment() {
        return buyerComment;
    }
    
    /**
     * 设置买家是否已经评价
     * @param buyerComment 买家是否已经评价
     */
    public void setBuyerComment(Integer buyerComment) {
        this.buyerComment = buyerComment;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.OrderEntity").append("ID="+this.getId()).toString();
    }
}

