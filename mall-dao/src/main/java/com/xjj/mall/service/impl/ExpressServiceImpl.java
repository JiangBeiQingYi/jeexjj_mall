/****************************************************
 * Description: ServiceImpl for 商品描述表
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.ExpressEntity;
import com.xjj.mall.dao.ExpressDao;
import com.xjj.mall.service.ExpressService;

@Service
public class ExpressServiceImpl extends XjjServiceSupport<ExpressEntity> implements ExpressService {

	@Autowired
	private ExpressDao expressDao;

	@Override
	public XjjDAO<ExpressEntity> getDao() {
		
		return expressDao;
	}
}