/****************************************************
 * Description: ServiceImpl for t_mall_log
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.LogEntity;
import com.xjj.mall.dao.LogDao;
import com.xjj.mall.service.LogService;

@Service
public class LogServiceImpl extends XjjServiceSupport<LogEntity> implements LogService {

	@Autowired
	private LogDao logDao;

	@Override
	public XjjDAO<LogEntity> getDao() {
		
		return logDao;
	}
}